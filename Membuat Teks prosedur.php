<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Teks Prosedur Membuat Telur Ceplok</title>
    <link rel="stylesheet" type="text/css" href="stylesheet.css">

</head>
<body>
  <div> <nav  class="nav">
    <div class="wrap">
    <section class="title">ZAHYHABIBI</section>
<div class="menu">
<ul class="navbar-nav">
    <li><a href="">home</a></li>
    <li><a href="">Result Page</a></li>
    <li><a href="">Portofolio</a></li>
    <li><a href="Youtube.com">Youtube</a></li>
    </ul>
</div>

</div>
    </nav>
</div>
<div>

<div class="titles">

<h1>Cara membuat telur Ceplok</h1>
    <H3>Bahan-Bahan</H3>
    <ul>
        <li>1 Butir Telur</li>
        <li>3 Sendok Makan Minyak kelapa</li>
        <li>setengah sendok teh penyedap rasa / garam</li>
        <li>sepiring nasi dan satu sendok</li>
    </ul>
    <H3>Alat Yang Dibutuhkan</H3>
        <ul>
            <li>Kompor</li>
            <li>gas</li>
            <li>susuk</li>
            <li>teflon / katel</li>
        </ul>
        <h1>Cara Pembuatan</h1>
        <ul>
            <li>pasang terlebih dahulu gas ke kompor supaya bisa menyalakan api</li>
            <li>setelah terpasang tekan tuas lalu putar ke arah kiri supaya api bisa keluar dan menyala</li>
            <li>simpan teflon / katel di atas tungku api yang sedang menyala lalu masukan 3 sendok makan minyak kelapa, tunggu minyak hingga panas</li>
            <li>pecahkan telur dan masukan kedalam teflon / katel yang berisi minyak panas dan jangan lupa beri setengah sendok teh penyedap rasa / garam</li>
            <li>tunggu telur ceplok hingga matang, sampai lendir telur menjadi warna putih dan bertekstur</li>
            <li>setelah matang ambil sepiring nasi dan satu sendok, dan simpan telur yang sudah matang kedalam piring yang berisi nasi dan sebuah sendok</li>
            <li>sepiring telur ceplok siap disantap ^_^</li>
        </ul>
        <h1>Kegiatan Kegiatan yang Dilakukan saat Proses pembuatan telur ceplok</h1>
        <ul>
            <li>menyiapkan telur dan bahan bahan lainnya</li>
            <li>menyiapkan alat-alat yang tertera di atas</li>
            <li>memasang gas ke kompor</li>
            <li>menekan tuas ke kiri supaya kompor menyala</li>
            <li>menyimpan katel kedalam tungku api</li>
            <li>menyimpan 3 sendok minyak kelapa</li>
            <li>memecahkan telur</li>
            <li>memasukan telur ke dalam teflon / katel</li>
            <li>memberi menyedap rasa / garam</li>
            <li>menggoreng telur</li>
            <li>menunggu hingga matang</li>
            <li>menyimpan nasi kedalam piring</li>
            <li>menyimpan telur kedalam piring</li>

        </ul>
<ul>        <li>menyiapkan (telur,minyak, nasi, sendok,kompor,gas,susuk,teflon)</li>
            <li>memasang (gas)</li>
            <li>menekan (tuas)</li>
            <li>menyimpan (katel)</li>
            <li>menyimpan (minyak)</li>
            <li>menunggu (panas)</li>
            <li>memecahkan (telur)</li>
            <li>memasukan (telur)</li>
            <li>memberi (garam)</li>
            <li>menunggu  (matang)</li>
            <li>menyimpan (nasi , telur)</li>
</ul>
</div>    
</div>
</body>
</html>